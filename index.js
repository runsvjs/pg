'use strict';
const {
	Pool
} = require('pg');
const name = 'pg';

function create(options) {
	let pool;

	function _start(callback) {
		pool = new Pool(options);
		pool.connect((err, client, release) => {
			if (err) {
				return callback(err);
			}
			release();
			return callback();
		});
	}

	function start(_, callback) {
		if (pool) {
			return callback();
		}
		_start(callback);
	}

	function stop(callback) {
		pool.end(function (err) {
			pool = null;
			return callback(err);
		});
	}

	function getClient() {
		return pool;
	}
	return Object.freeze({
		name,
		start,
		stop,
		getClient
	});
}
exports = module.exports = create;
