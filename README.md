[![pipeline status](https://gitlab.com/runsvjs/pg/badges/master/pipeline.svg)](https://gitlab.com/runsvjs/pg/commits/master)
[![coverage report](https://gitlab.com/runsvjs/pg/badges/master/coverage.svg)](https://gitlab.com/runsvjs/pg/commits/master)
# runsv PostgreSQL service 

This is a service wrapper around the excellent [node-postgres](https://node-postgres.com/) for [runsv](https://www.github.com/runsvjs/runsv).

## Install

If you have already installed `pg`  
```
$ npm install runsv-pg
```
Otherwise
```
$ npm install pg runsv-pg
```

> pg is a [peer dependency](https://nodejs.org/es/blog/npm/peer-dependencies/) of this module


## Usage
```javascript
const runsv = require('runsv')();
const pg = require('runsv-pg')();

runsv.addService(pg);
runsv.start(function(err, {pg}){
	pg.query('SELECT $1::text as message;', ['hello'], function(err, res){
	//your program goes here
	});
});
```

## Configure node-postgres

You can configure your connection to PostgreSQL the [same way you](https://node-postgres.com/features/connecting) you do it with `node-postgres`. You can use the environment variables or pass your configuration options.

```javascript
// Passing config options

const pgConfig = {
...
};
const pg = require('runsv-pg')(pgConfig);
```

## 


