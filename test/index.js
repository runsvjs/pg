'use strict';
require('dotenv').config();
const runsv = require('runsv');
const assert = require('assert');
const createService = require('..');
const sql = 'SELECT $1::text as message';
const sqlParams = ['Hello world!'];
describe('service', function () {
	describe('#start()/#stop() behavior', function () {
		before(function (done) {
			const sv = runsv.create();
			sv.addService(createService());
			sv.start(done);
			this.sv = sv;
		});
		after(function (done) {
			this.sv.stop(done);
		});
		it('should be compatible with runsv', function (done) {
			const {
				sv
			} = this;
			let {
				pg
			} = sv.getClients();
			pg.connect(function (err, client, release) {
				if (err) {
					return done(err);
				}
				client.query(sql, sqlParams, function (err, res) {
					release();
					if (err) {
						return done();
					}
					const [expected] = sqlParams;
					const actual = res.rows[0].message;
					assert.deepStrictEqual(actual, expected);
					done();
				});
			});
		});
	});
});
